FROM nginx:1.13.3-alpine

COPY nginx/nginx.conf /etc/opt/rh/rh-nginx112/nginx/

## Remove default nginx website
RUN rm -rf /opt/app-root/src/*

## copy over the artifacts in dist folder to default nginx public folder
COPY dist/ /opt/app-root/src

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
